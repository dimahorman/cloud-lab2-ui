import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Farm} from '../tab-content-farm/farm';
import {FarmField} from '../tab-content-field/FarmField';
import {Sensor} from '../tab-content-sensor/Sensor';
import {Farmer} from '../tab-content-farmer/Farmer';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FarmService {
  private readonly url = environment.url;

  constructor(private httpClient: HttpClient) {
  }

  public getFarmer(name: string): Observable<HttpResponse<Farmer>> {
    return this.httpClient.get<Farmer>(this.url + '/farmers/' + name, {
      observe: 'response'
    });
  }

  public getFarmers(): Observable<HttpResponse<Array<Farmer>>> {
    return this.httpClient.get<Array<Farmer>>(this.url + '/farmers', {
      observe: 'response'
    });
  }

  public getFarm(id: string): Observable<HttpResponse<Farm>> {
    return this.httpClient.get<Farm>(this.url + '/farms/' + id, {
      observe: 'response'
    });
  }

  public getFarmsByFarmerName(farmerName: string): Observable<HttpResponse<Array<Farm>>> {
    return this.httpClient.get<Array<Farm>>(this.url + '/farmers/' + farmerName + '/farms', {
      observe: 'response'
    });
  }

  public getFarmField(id: number): Observable<HttpResponse<FarmField>> {
    return this.httpClient.get<FarmField>(this.url + '/fields/' + id, {
      observe: 'response'
    });
  }

  public getFarmFieldsByFarmId(farmId: string): Observable<HttpResponse<Array<FarmField>>> {
    return this.httpClient.get<Array<FarmField>>(this.url + '/farms/' + farmId + '/fields', {
      observe: 'response'
    });
  }

  public getSensor(id: number): Observable<HttpResponse<Sensor>> {
    return this.httpClient.get<Sensor>(this.url + '/sensors/' + id, {
      observe: 'response'
    });
  }

  public getSensorsByFarmId(farmId: string): Observable<HttpResponse<Array<Sensor>>> {
    return this.httpClient.get<Array<Sensor>>(this.url + '/farms/' + farmId + '/sensors', {
      observe: 'response'
    });
  }

  public getSensorsByFieldId(fieldId: number): Observable<HttpResponse<Array<Sensor>>> {
    return this.httpClient.get<Array<Sensor>>(this.url + '/fields/' + fieldId + '/sensors', {
      observe: 'response'
    });
  }
}
