import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabContentFarmerComponent } from './tab-content-farmer.component';

describe('TabContentFarmerComponent', () => {
  let component: TabContentFarmerComponent;
  let fixture: ComponentFixture<TabContentFarmerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabContentFarmerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabContentFarmerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
