import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabContentFieldComponent } from './tab-content-field.component';

describe('TabContentFieldComponent', () => {
  let component: TabContentFieldComponent;
  let fixture: ComponentFixture<TabContentFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabContentFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabContentFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
